import { de } from '@formkit/i18n'
import { defineFormKitConfig } from '@formkit/vue'
import { generateClasses } from '@formkit/themes'
import theme from './formkit.theme'

export default defineFormKitConfig({
  config: {
    classes: generateClasses(theme),
  },
  locales: { de },
  locale: 'de',
})
