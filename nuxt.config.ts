// https://v3.nuxtjs.org/api/configuration/nuxt.config
// @ts-ignore
import svgLoader from 'vite-svg-loader'

export default defineNuxtConfig({
  css: ['~/assets/css/main.css'],

  modules: [
    // https://vueuse.org/nuxt/readme.html
    '@vueuse/nuxt',
    // https://v8.i18n.nuxtjs.org/
    '@nuxtjs/i18n',
    // https://ui.nuxt.com/getting-started/installation
    '@nuxt/image',
    // https://ui.nuxt.com/getting-started/installation
    '@nuxt/ui',
    // https://formkit.com/getting-started/installation#with-nuxt
    '@formkit/nuxt',
    // https://nuxt.com/modules/vue-email
    '@vue-email/nuxt',
  ],

  image: {
    dir: 'assets/images',
    provider: 'ipx',
    format: ['webp', 'jpg'],
    quality: 90,
    screens: {
      xs: 320,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
      xxl: 1536,
      '2xl': 1536,
    },
  },

  app: {
    head: {
      htmlAttrs: {
        dir: 'ltr',
        lang: 'de',
      },
      meta: [
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1, viewport-fit=cover',
        },
      ],
    },
  },

  i18n: {
    strategy: 'prefix_except_default',
    // @ts-ignore
    vueI18nLoader: true,
    locales: [
      {
        code: 'de',
        iso: 'de-DE',
        name: 'de',
      },
      {
        code: 'en',
        iso: 'en-US',
        name: 'en',
      },
    ],
    defaultLocale: 'de',
  },

  vite: {
    plugins: [svgLoader()],
    ssr: {
      noExternal: [],
    },
    server: {
      watch: {
        usePolling: true,
      },
    },
  },

  formkit: {
    autoImport: true,
  },

  runtimeConfig: {
    public: {
      APP_NAME: process.env.APP_NAME ?? 'Placeholder',
    },
  },

  imports: {
    dirs: ['./constants'],
  },

  tailwindcss: {
    configPath: './tailwind.config.ts',
  },

  devtools: { enabled: true },
  components: true,
})
